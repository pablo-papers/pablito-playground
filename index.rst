|Documentation Status|

.. |Documentation Status| image:: https://readthedocs.org/projects/pablito-playground/badge/?version=latest
   :target: http://pablito-playground.readthedocs.io/?badge=latest

Welcome to Pablito's playground
===============================

.. toctree::
   :maxdepth: 2
   :caption: Cheat codes

   cheat_codes/useful
   cheat_codes/after_install_linux
   cheat_codes/compile_cpmd
   cheat_codes/python_format_str
   cheat_codes/cpmd_regtests
   cheat_codes/lsdalton_memo
   cheat_codes/gimp

.. toctree::
   :maxdepth: 2
   :caption: Tutorials

   tutorials/qmmm_amber_cpmd
   tutorials/cpmd_plot_orbitals

Links
-----

* Documentation of the comp_chem_py_ library: https://comp-chem-py.readthedocs.io
  
* Introduction to Coupled Cluster Theory: https://cc-lecture.readthedocs.io

* VMD Quick help page (very useful when writing vmd scripts): http://www.ks.uiuc.edu/Research/vmd/vmd_help.html


.. _comp_chem_py: https://gitlab.com/pablobaudin/comp_chem_py

Publications
------------

A complete list of my scientific publications can be found on:

* My google-scholar account: https://scholar.google.com/citations?user=ZKpbiOUAAAAJ

* My ORCID account: https://orcid.org/0000-0001-7233-645X

* :download:`My PhD thesis: <phd_thesis_pablo_baudin.pdf>` "Coupled Cluster theory for large molecules".


.. toctree::
    :maxdepth: 2
    :caption: Stuff not work related (in french)
    
    fun/blog


.. _contact:

Contact
-------

* Pablo Baudin
* Scientific collaborator at LCBC, EPFL: https://lcbc.epfl.ch
* email: pablo.baudin@epfl.ch

..
    Indices and tables
    ------------------
    
    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
