Voyage de Roxane et Pablo
=========================

* Site du Northern Farm Training Institute pour du benevolat au nord du Canada: http://nftinwt.com/

* Site avec des recits de voyages: https://www.lesothers.com/category/aventures/

Site web pour voyager en bateau
-------------------------------

* https://www.vogavecmoi.com/

* https://www.crewseekers.net/

* https://stw.fr/

* https://www.allcruisejobs.com/

* https://cruiseplacement.com/

Direction canada
----------------

* Benevolat avec greepeace: https://www.greenpeace.org/international/act/sail-aboard-a-greenpeace-ship/

* Benevolat avec sea sheperd: https://www.seashepherdglobal.org/get-involved/volunteer-sea/

* Ferry depuis le danemark vers l'island, puis... greenland... https://www.lonelyplanet.com/scandinavia/travel-tips-and-articles/sailing-from-denmark-to-iceland


