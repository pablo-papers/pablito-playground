Le complot de la connaissance
=============================

Sur ces pages j'aimerais constituer une bibliothèque des médias que je consulte
au quotidien (vidéos, podcasts, articles, livres...) et qui me plaise
particulièrement. 

Je voudrais me concentrer sur certains sujet aussi divers que la mécanique
quantique, l'économie, la philosophie morale, la permaculture ou le féminisme.

Ce qui m'intéresse particulièrement c'est de pouvoir garder une trace des
contenus qui me plaisent et de créer du lien entre tout ces domaines à la
manière d'une théorie du complot.

Mon univers de la connaissance
------------------------------

.. raw:: html
   :file: circular_packing_interactive.html

