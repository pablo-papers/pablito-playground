.. _leap_non_standard_system:

Leap (Amber): set up a non-standard system from a .pdb file
===========================================================

This tutorial explains how to obtain input files for Amber, 
from a ``.pdb`` file containing residues not automatically recognized 
by Amber force fields.

An example of this might be when you generate a ``.pdb`` file from 
packmol as described in :ref:`this tutorial <packmol_custom_box>`.

We will therefore assume that we have a system.pdb which contains 
three types of molecules, with the structure described into solute.pdb, 
solvent1.pdb, and solvent2.pdb, and residue names SLU, SV1, and SV2, 
respectively.

In addition, we will need the ``.mol2`` and ``.frcmod`` files for all 
three species. These can be obtained as described in 
:ref:`this tutorial <mol2_file>`.

Once all the required files have been produced, a simple input file for 
leap can be written and executed to generate inputs for Amber.

**leap.inp**::

    source leaprc.ff14SB
    source leaprc.gaff
    #
    # load solute
    SLU = loadmol2 solute.mol2
    loadamberparams solute.frcmod
    check SLU
    #
    # load solvent1
    SV1 = loadmol2 solvent1.mol2
    loadamberparams solvent1.frcmod
    check SV1
    #
    # load solvent2
    SV1 = loadmol2 solvent2.mol2
    loadamberparams solvent2.frcmod
    check SV2
    #
    # load system
    SYS = loadpdb system.pdb
    # The dimension of the box should match those used to create system.pdb with packmol
    set SYS box {30,30,30}
    saveamberparm SYS system.top system.rst
    quit

Be sure to adapt the name of the input (and output) files in the script, 
as well as the size of the box (it should match the box size used in packmol).

Finally, we just need to load Amber and run leap::

    module load amber/14
    tleap -f leap.inp -s > leap.out &

Check that everything went fine in the leap.out file and visualize the result in vmd::

    module load vmd
    vmd -parm7 system.top -rst7 system.rst

