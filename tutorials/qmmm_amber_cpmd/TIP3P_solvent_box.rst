.. _TIP3P_solvent_box:

Leap (Amber): solvate molecule in TIP3P water
=============================================

This tutorial explains how to generate input files for Amber (topology and structure) 
for a molecule solvated in water. It assumes that a complete ``.mol2`` file is available 
for the solute (``solute.mol2``). An additional ``solute.frcmod`` file might be required.

Check :ref:`this tutorial <mol2_file>` to see how to get the ``.mol2`` and ``.frcmod`` files.

From here, everything can be done in one step, by generating an input for the leap program.

**leap.inp**::

    # load parameters
    source leaprc.ff14SB
    source leaprc.gaff
    #
    # load solute molecule
    SLU = loadmol2 solute.mol2
    list
    loadamberparams solute.frcmod
    check SLU
    #
    # solvate solute in water (TIP3P)
    # 14 is the minimal distance (in Angstroms) between
    # the solute and the edge of the box
    solvatebox SLU TIP3PBOX 14
    #
    # save structure
    saveamberparm SLU solute_solv.top solute_solv.rst
    quit

Be sure to adapt the name of the input (and output) files in the script, as well as 
the size of the box (here we have a minimal distance of 14 Angstroms between the 
solute and the edge of the box). You can also chose another standard solvent 
(displayed after the list command in tleap). 
Check :ref:`this tutorial <custom_solvent_box>` to use a custom solvent box.

Finally, we just need to load Amber and run leap::

    module load amber/14
    tleap -f leap.inp -s > leap.out &

Check that everything went fine in the ``leap.out`` file and visualize 
the result in vmd::

    module load vmd
    vmd -parm7 solute_solv.top -rst7 solute_solv.rst

