.. _mol2_file:

Generate .mol2 file for leap and Amber
======================================

This tutorial explains how to generate a ``.mol2`` file and eventually a 
``.frcmod`` file that can be used by leap to create input files for
molecular dynamics with Amber.

.. note::

    When setting up files for Amber calculations, the residue names can be 
    of great importance. Try to be as consistent as possible during those 
    steps with the name of the residues (in the original ``.pdb`` file also 
    use the ``-rn`` option of antechamber).

Method 1: directly from a .pdb file
------------------------------------

You can use the antechamber program to generate a ``.mol2`` file from a 
``molecule.pdb`` file. Just run antechamber as follows::

    module load amber/14
    antechamber -i molecule.pdb -fi pdb -o molecule.mol2 -fo mol2 -c bcc -nc 0 -m 1

Method 2: Use RESP charges from Gaussian
----------------------------------------

It might be better to use RESP charges obtained from a Gaussian calculations to 
generate the ``.mol2`` file (see :ref:`this tutorial <RESP_charges>` for the 
generation of RESP charges). 
In this case the ``.mol2`` file is obtained by calling antechamber 
in the following way::

    antechamber -i electrostatic_grid.log -fi gout -o solute.mol2 -fo mol2 -c resp -nc 0 -m 1 -rn MOL

Extra parameters
----------------

Some extra parameters might be required. They can be obtained in a ``.frcmod`` file 
with parmchk as::

    parmchk -i solute.mol2 -f mol2 -o solute.frcmod

Antechamber options
-------------------

It might be convenient to look at the options from antechamber::

    antechamber -h


