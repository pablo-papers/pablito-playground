.. _packmol_custom_box:

Generate custom box with packmol
================================

This tutorial explains how to generate a box (``.pdb`` file) with 
different species (e.g. a solute and a solvent mixture).

Lets assume we want to generate a cubic box  (30x30x30 Angstroms) 
containing a solute molecule in its center and surrounded by a mixture 
of two different molecules (e.g. water and ethanol).

#. Generate a ``.pdb`` file for the individual species 
    (solute.pdb, solvent1.pdb, and solvent2.pdb). This can be done with 
    chimera_ using an ``.xyz`` file.

    .. note::

        Make sure the molecules have different residue names in the .pdb 
        files. Lets say SLU, SV1, and SV2.

#. Create an input file (``packmol.inp``) for packmol_ which will 
    generate a ``.pdb`` file for the whole solvated system (``system.pdb``).

    **packmol.inp**::
    
        # All atoms from diferent molecules will be at least 2.0 Angstroms apart
        tolerance 2.0
        
        # The type of the files will be pdb
        filetype pdb
        
        # The name of the output file
        output system.pdb
        
        # put the COM of the solute at the center of the box
        structure solute.pdb
          number 1
          fixed 15. 15. 15. 0. 0. 0.
          centerofmass
        end structure
        
        # add first type of solvent molecules
        structure solvent1.pdb
          number 100
          inside cube 0. 0. 0. 30
        end structure
        
        # add second type of solvent molecules
        structure solvent2.pdb
          number 100
          inside cube 0. 0. 0. 30
        end structure
    
    .. note::
    
        Be sure to modify the dimension of the box, the number of molecules to add, 
        and the file names to your convenience.
    
#. Run packmol::

	./pathto/packmol < packmol.inp

If you have put to many molecules in the box, packmol might fail to converge. 
For more information, check the packmol_ documentation.

.. _packmol: http://m3g.iqm.unicamp.br/packmol/home.shtml
.. _chimera: https://www.cgl.ucsf.edu/chimera/

