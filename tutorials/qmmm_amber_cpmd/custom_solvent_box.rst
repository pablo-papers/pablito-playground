.. _custom_solvent_box:

Leap (Amber): custom solvent box
================================

This tutorial explains how to generate and use a custom solvent box to solvate a 
molecule for an MD simulation with Amber.

.. note::

   Instead of creating a custom solvent box, it might be better to directly set 
   up the whole system (solute + solvent) as described in :ref:`this tutorial <packmol_custom_box>` and :ref:`that one <leap_non_standard_system>`.
   It is therefore not recommended to follow this tutorial!

#. Generate the box with packmol

    In this step we want to generate a ``.pdb`` file for the (non-optimized) solvent box.

    This can be done by following :ref:`this tutorial <packmol_custom_box>` and using only one species in packmol (e.g. solvent1).

#. Creating input files for Amber

    Now that we have a first structure of the solvent box, we need to equilibrate it. 
    For that we will use the Amber package. We thus need to create input files for Amber.

    This can be done by following :ref:`this tutorial <leap_non_standard_system>` with a single type of molecules (the solvent).

#. Equilibrate the custom box

    In this step we use the input files generated in the previous step to equilibrate the box.

    This can be done by following :ref:`this tutorial <amber_equilibrate_system>`.

#. Save custom box in library file

    Now that we have equilibrated our box. We will use leap to generate a library file for the box. 
    This step requires to have a ``.pdb`` file of the equilibrated structure as well as a ``.mol2`` 
    file of the solvent molecule.

    .. note:: 
        
        The residue name of the solvent molecule must be the same in the two files and in the script below (SLV).

    **leap.inp**::
    
        # load parameters
        source leaprc.ff14SB
        source leaprc.gaff
        #
        # load solvent molecule parameters
        SLV = loadmol2 solvent.mol2
        loadamberparams solvent.frcmod
        check SLV
        #
        # load equilibrated pdb file
        MYBOX = loadpdb system.pdb
        #
        # set the dimensions of the box (check in pdb file)
        set MYBOX box {30,30,30}
        #
        # save box as library file
        saveoff MYBOX .my_box.lib
        quit

    Just load Amber and run leap::

        module load amber/14
        tleap -f leap.inp -s > leap.out &

 
#. Use a custom box

    Now that our box has been equilibrated, we can use it to solvate a molecule. 
    Create an input file for leap (adapt it if needed). For this step you need 
    to have a ``.mol2`` and a ``.frcmod`` file for the solute (see 
    :ref:`this tutorial <mol2_file>`  to generate those files).

    **leap.inp**::

        # load parameters
        source leaprc.ff14SB
        source leaprc.gaff
        #
        # load solute molecule
        SLU = loadmol2 solute.mol2
        loadamberparams solute.frcmod
        check SLU
        #
        # load the custom box library. 
        # The box has been generated with the name MYBOX (see below)
        loadoff my_box.lib
        #
        # solvate solute in custom solvent
        # 14 is the minimal distance (in Angstroms) between 
        # the solute and the edge of the box 
        solvatebox SLU MYBOX 14
        #
        # save structure
        saveamberparm SLU solute_solv.top solute_solv.rst 
        quit

    Just load Amber and run leap::

        module load amber/14
        tleap -f leap.inp -s > leap.out &

    Check that everything went fine and visualize::

        vmd -parm7 solute_solv.top -rst7 solute_solv.rst

