.. _RESP_charges:

Generate RESP charges with Gaussian
===================================

In case one uses molecules unrecognized by Amber, a restrained 
electrostatic potential (RESP) charges calculation might be needed. 
Here is how to do it with the Gaussian package.

First optimize the structure of the desired molecules. Here we use 
n-hexane and optimize it at the  DFT/B3LYP/6-31G(d,p) level using 
16 cores and 4000MB of memory per core.

**geo_opt.com**::
    
    %NProcShared=16
    %chk=./geo_opt.chk
    %mem=4000MB
    #p opt b3lyp/6-31G(d,p) nosymm iop(6/7=3) gfinput
    n-hexane geometry optimization
    
    0 1
    C 0.7746 -0.0257 0.0000
    C -0.7746 0.0257 0.0000
    C 1.3570 -1.4627 0.0000
    C -1.3570 1.4627 0.0000
    C 2.9005 -1.5027 -0.0000
    C -2.9005 1.5027 -0.0000
    H 1.1449 0.5037 0.8825
    H 1.1449 0.5037 -0.8825
    H -1.1449 -0.5037 -0.8825
    H -1.1449 -0.5037 0.8825
    H 0.9985 -2.0010 0.8818
    H 0.9985 -2.0010 -0.8818
    H -0.9985 2.0010 0.8818
    H -0.9985 2.0010 -0.8818
    H 3.2506 -2.5369 0.0000
    H 3.2996 -1.0073 -0.8871
    H 3.2996 -1.0073 0.8871
    H -3.2506 2.5369 0.0000
    H -3.2996 1.0073 -0.8871
    H -3.2996 1.0073 0.8871

Run Gaussian as follows::

    module load g09
    nohup g09 < geo_opt.com > geo_opt.log &

Once the geometry has been optimized we can calculate the RESP charges 
with the following input.

**electrostatic_grid.com**::
    
    %NProcShared=16
    %chk=./electrostatic_grid.chk
    %mem=4000MB
    #p b3lyp/6-31G(d,p) nosymm iop(6/33=2) pop(chelpg,regular)
    
    n-hexane electrostatic grid calculation
    
    0 1
    C   0.766190   -0.024729    0.000000
    C  -0.766190    0.024729    0.000000
    C   1.335815   -1.448705    0.000000
    C  -1.335815    1.448705    0.000000
    C   2.866858   -1.488377    0.000000
    C  -2.866858    1.488377    0.000000
    H   1.147135    0.517440    0.877538
    H   1.147135    0.517440   -0.877538
    H  -1.147135   -0.517440   -0.877538
    H  -1.147135   -0.517440    0.877538
    H   0.955507   -1.990023    0.877033
    H   0.955507   -1.990023   -0.877033
    H  -0.955507    1.990023    0.877033
    H  -0.955507    1.990023   -0.877033
    H   3.241978   -2.516956    0.000000
    H   3.275854   -0.985176   -0.883579
    H   3.275854   -0.985176    0.883579
    H  -3.241978    2.516956    0.000000
    H  -3.275854    0.985176   -0.883579
    H  -3.275854    0.985176    0.883579

And the following command::

    nohup g09 < electrostatic_grid.com > electrostatic_grid.log &

Where we have copied the geometry from the previous calculation. The 
``electrostatic_grid.log`` output file can then be used, e.g. to 
:ref:`generate a .mol2 file for leap and Amber <mol2_file>`.

