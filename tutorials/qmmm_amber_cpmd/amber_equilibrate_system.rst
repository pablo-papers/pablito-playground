.. _amber_equilibrate_system:

Amber: equilibrate system
=========================

This tutorial explains how to equilibrate a system using the Amber package. 
It assumes that the input files describing the system (topology system.top a
nd coordinates system.rst) have already been generated.

* :ref:`This tutorial <TIP3P_solvent_box>` explains how to generate the system files for a molecule in TIP3P water.
* :ref:`This tutorial <leap_non_standard_system>` explains how to generate the system files for a system with non-standard residues (using packmol).

The equilibration of the system will be done in four main steps:

#. In case of a solvated system: a restrained minimization (Optional).
#. A global minimization.
#. Heating the system to the desired temperature.
#. Equilibration with Thermostat and Barostat (NPT simulation).
#. Re-imaging: to use the equilibrated system with other programs (Optional).


In the following, we collect the commands used to submit the different
calculations. The input files can be downloaded below.

In the input files the main information to be changed is the number of cycles or the 
temperature or pressure. In the command lines, the file names need to be adapted as well 
as the number of MPI processes.

First thing to do::

    module load amber/14

#. Restrained minimization (Optional)

    * Amber input file: :download:`1-restraint.inp <downloads/1-restraint.inp>`
    * Command line::
    
        nohup mpirun -np 24 sander.MPI -O -i 1-restraint.inp -o eq_restraint.out -c system.rst -p system.top -r eq_restraint.rst -ref system.rst -inf eq_restraint.info &

#. Minimization

    * Amber input file: :download:`2-minimization.inp <downloads/2-minimization.inp>`
    * Command line::
    
        nohup mpirun -np 24 sander.MPI -O -i 2-minimization.inp -o eq_minimization.out -c eq_restraint.rst -p system.top -r eq_minimization.rst -inf eq_minimization.info &

#. Heating

    * Amber input file: :download:`3-heating.inp <downloads/3-heating.inp>`
    * Command line::
    
        nohup mpirun -np 24 sander.MPI -O -i 3-heating.inp -p system.top -c eq_minimization.rst -ref eq_minimization.rst -o eq_heating.out -r eq_heating.rst -x eq_heating.crd -e eq_heating.en -inf eq_heating.info &

#. Equilibration

    * Amber input file: :download:`4-equilibration.inp <downloads/4-equilibration.inp>`
    * Command line::
    
        nohup mpirun -np 24 sander.MPI -O -i 4-equilibration.inp -p system.top -c eq_heating.rst -o eq_density.out -r eq_density.rst -x eq_density.crd -e eq_density.en -inf eq_density.info &

#. Re-imaging (Optional)

    * Amber input file: :download:`5-reimaging.inp <downloads/5-reimaging.inp>`
    * Command line::
    
        cpptraj system.top < 5-reimaging.inp
    
    If you need to get a ``.pdb`` file of the equilibrated system, open it 
    with vmd and save it using the pdb format::
    
        vmd -parm7 system.top -rst7 eq_density_reimaged.rst

