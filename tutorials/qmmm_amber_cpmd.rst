.. _qmmm_amber_cpmd: 

Equilibration at MM and QMMM level using Amber and CPMD
=======================================================

This is a collection of tutorials that I wrote for myself. I find it convenient to go 
back here and read and update my workflow on the way. In this way I also keep track of what 
I did in the past and how I can improve it.

.. note::  Use at your own risk!

    I am not an expert in everything written here! Some of the tutorials 
    might contain commands and workflows that are not optimal at all or 
    not recommended. If you have better ways of doing what I present here, 
    or if you want to comment or share your own tricks, fell free to 
    :ref:`contact me <contact>`. 

    Thanks!


For now the tutorials describe very short workflows that can be used to equilibrate systems 
with the Amber and CPMD packages. The material presented here is an adaptation and decomposition of 
a QM/MM tutorial by E. Ippoliti and J. Dreyer (`Tutorial: QM/MM MD by CPMD`_).

.. _`Tutorial: QM/MM MD by CPMD`: http://www.grs-sim.de/research/computational-biophysics/internals/qmmm-cpmd.html


.. toctree::
    :maxdepth: 1
    
    ./qmmm_amber_cpmd/RESP_charges
    ./qmmm_amber_cpmd/mol2_file
    ./qmmm_amber_cpmd/TIP3P_solvent_box
    ./qmmm_amber_cpmd/packmol_custom_box
    ./qmmm_amber_cpmd/leap_non_standard_system
    ./qmmm_amber_cpmd/custom_solvent_box
    ./qmmm_amber_cpmd/amber_equilibrate_system
    ./qmmm_amber_cpmd/cpmd_qmmm


