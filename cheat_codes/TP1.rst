Données expérimentatles du TP 1 
===============================

* Le fichier ci-dessous contient les données expérimentales du TP 1.

* Format : Nombre de pièces | Volume | masse | masse volumique

.. code-block:: bash

    4 2 15.85 7.925
    5 4 27.62 6.905
    4 2 15.68 7.84
    3 2 11.8  5.9
    1 0.5 3.9 7.8
    3 1 11.93 11.93
    4 2 15.81 7.905
    8 5 31.5  6.3
    4 3 16    5.333
    5 2 19.74 9.87
    6 3 23.5  7.833
    5 3 19.66 6.553
    6 3 23.57 7.857
    5 2 19.7  9.85


:download:`data.txt<downloads/data.txt>`
