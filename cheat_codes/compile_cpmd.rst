.. _compile_cpmd: 

Compile CPMD
============

Instructions on how to compile CPMD

Cloning CPMD
------------

The svn version of CPMD can be obtained as follows (on the LCBC cluster it 
requires to log on station 66 or 56 to get svn working):

.. code-block:: bash

   svn co --username bpj http://www.cpmd.org:81/repos/trunk cpmd

A current version can be updated with,

.. code-block:: bash

   svn update

On the LCBC cluster at EPFL
---------------------------

This compilation is for production runs, it is an MPI version and it uses the 
intel compiler (the configuration file :download:`LINUX-X86_64-INTEL-IMPI-LXC-CLUSTER 
<downloads/LINUX-X86_64-INTEL-IMPI-LXC-CLUSTER>` should be in 
be in ``[CPMD_ROOT_DIR]/CPMD/configure/``).

#. Change directory to the main directory of the CPMD code) if you just cloned
   CPMD as stated above,

   .. code-block:: bash

      cd cpmd/CPMD/

#. Load modules

   .. code-block:: bash

      module load intel/15.0.3 intelmpi/5.1.1

#. Configure (enable QMMM, executable will be in ``bin_intel/bin/cpmd.x``)

   .. code-block:: bash

      ./configure.sh -qmmm -DEST=bin_intel LINUX-X86_64-INTEL-IMPI-LXC-CLUSTER

   With new versions of CPMD it can be needed to set the maximum stack size to 
   unlimited:

   .. code-block:: bash

      ulimit -s unlimited

#. Finally, compile CPMD (here with 16 cores):

   .. code-block:: bash

      cd bin_intel
      make -j16

On my laptop
------------

This compilation is for debugging, it is a serial version and it uses the 
gfortan-7 compiler (the configuration file :download:`LINUX-X86_64-GFORTRAN-LAPTOP
<downloads/LINUX-X86_64-GFORTRAN-LAPTOP>`
should be in ``[CPMD_ROOT_DIR]/CPMD/configure/``).

#. Make sure that the mkl library is available (should be in ``.bashrc``
   file):

   .. code-block:: bash

      source /opt/intel/composer_xe_2015.2.164/mkl/bin/mklvars.sh intel64

#. From the CPMD main directory, configure and compile:

   .. code-block:: bash
      
      ./configure.sh -debug -DEST=bin_gfortan LINUX-X86_64-GFORTRAN-LAPTOP
      cd bin_gfortran
      make -j8

