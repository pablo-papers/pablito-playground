LSDalton memo
=============

* Insert new test cases in:

.. code-block:: bash

   ${LSDALTON_ROOT}/cmake/TestsLSDALTON.cmake

* When Adding new source files:

   #. Add ``newfile.o`` in the ``Makefile`` present in the directory containing the new file.

   #. Add ``path/to/file.f90`` to ``${LSDALTON_ROOT}/cmake/SourcesLSDALTON.cmake``.

For more information or to add directories see: ``${LSDALTON_ROOT}/src/README_AddDirectory``.

For debugging memory issues with valgrind:

.. code-block:: bash

   valgrind --leak-check=yes prog.exe > valgrind.out 2>&1 &

For other information go to: http://daltonprogram.org/

