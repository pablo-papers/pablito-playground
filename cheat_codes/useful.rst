Useful commands
===============

This section contains a collection of useful commands that I don't use enough
to remember.

Bash and general commands
-------------------------

* On Fujitsu LIFEBOOK E Series use the ``<F2>`` key to enter the bios.

* Find command:

.. code-block:: bash

   # Find files or directory with specific name:
   find [path] -name [\*part_of_filename*] -type f/d -print

   # Find file with specific name **and** which contains a specific string:
   find [path] -name [\*part_of_filename*] -type f -exec grep [string] '{}' \; -print

   # Remove all files except the ones matching a specific string:
   find . ! -name [\*part_of_filename*] -exec rm -rf {} +

* Merge different pdf files into a single one:

.. code-block:: bash

   pdftk in1.pdf in2.pdf cat output out1.pdf

* Create symbolic link:

.. code-block:: bash

   ln -s source_file myfile

* Tar compression and extraction:

.. code-block:: bash

   tar -czvf name-of-archive.tar.gz /path/to/directory-or-file
   tar -xzvf name-of-archive.tar.gz

* Managing packages:

.. code-block:: bash

   # update package list:
   sudo apt-get update

   # upgrade installed package:
   sudo apt-get upgrade

   # remove unused package:
   sudo apt-get autoremove

   # install package:
   sudo dpkg -i filename.deb

* Print the total disk space used inside the current directory::

   du -sh *

* Shutdown from command line::

   sudo /sbin/shutdown -r now

* To get ID of running jobs (on background)::

   jobs -l
   # or better
   ps -u pbaudin

* To produce a diff file between two directories, for example a patch for
  CPMD::

   diff -ruN -x '.svn' -x '.git' cpmd_new/ cpmd_ref/ > patch

Description of options: ``-r`` is for recursive, ``-u`` is to add 3 lines 
of context arround diffs, and ``-N`` is to treat absent files as empty. 
The ``-x`` allows to exclude files from the diff and should be repeated as 
many times as required.

* To print fortran code in a pretty format:
  
.. code-block:: bash

   a2ps --pretty-print=fortran --pro=color -Ppdf print.F90

* Open a file (e.g. .doc or .png) from command line using default application:

.. code-block:: bash

    gnome-open myfile
    # or 
    xdg-open myfile

It can be convenient to put the following alias in the ``.bashrc`` file:

.. code-block:: bash

    alias open='xdg-open'

Setting the cell dimension in vmd (usefull when plotting g(r))::

    pbc set {30.0 30.0 30.0} -all

Set up new computer cluster
---------------------------

#. Copy ``rc`` and other configuration files from laptop to cluster:

   .. code-block:: bash

      scp -r ~/.bashrc \
          ~/.git-completion.bash \
          ~/.gitignore_global \
          ~/.gitconfig \
          ~/.vimrc \
          ~/.vim/ \
          ~/.ssh/config \
          cluster:~

   Where ``cluster`` is the ssh label of the cluster from the ``~/.ssh/config`` file.

#. Generate ssh key for git:

   .. code-block:: bash
   
      ssh-keygen -t rsa -C "pablo.baudin@epfl.ch" -b 4096

#. Go to https://gitlab.com/profile/keys to add the key.


Git set up
----------

.. note::

    This is not needed if the gitconfig and other git files have been copied above

* Global settings when using git on new station:

.. code-block:: bash

   # general settings
   git config --global user.name "Pablo Baudin"
   git config --global user.email "pablo.baudin@epfl.ch"
   git config --global core.editor vim
   git config --global color.branch auto
   git config --global color.diff auto
   git config --global color.status auto
   git config --global core.excludesfile ~/.gitignore_global

   # Tools for merge and diff:
   git config --global diff.tool vimdiff
   git config --global diff.merge vimdiff
   git config --global difftool.prompt false

   # Alias (maybe too much?):
   git config --global alias.st status
   git config --global alias.d difftool

* Set the default mode for git push (current, matching or tracking):

.. code-block:: bash

   git config --global push.default current

* Disable fast-forward merges:

.. code-block:: bash

   git config branch.master.mergeoptions "--no-ff"

* Add the :download:`.git-completion.bash <downloads/.git-completion.bash>` file to your home.


Git commands
------------

* Create new branch:

.. code-block:: bash

   git checkout -b <local-branch-name>
   git push -u origin <local-branch-name>

If ``push.default = current`` is set then the ``-u`` option will track remote 
branch of same name.

* Delete branch:

.. code-block:: bash

   git push origin --delete <remote-branch-name>
   git branch -d <local-branch-name>

* Show all branches:

.. code-block:: bash

   git branch -a


Vim commands
------------

In the root directory of a ``fortran`` code, run

.. code-block:: bash
   
   ctags --fortran-kinds=+i -R *
   # or with the bash alias (inside the .bashrc file)
   ct

For a ``python`` code, run

.. code-block:: bash
   
   ctags --exclude=*.pyc --python-kinds=-i -R *
   # or with the bash alias (inside the .bashrc file)
   ctpy

This will create a tag file that can be used by ``vim``.
In your ``.vimrc`` file, insert the following lines:

.. code-block:: vim

   set tags=./tags;/
   map <C-h> :split <CR>:exec("tag ".expand("<cword>"))<CR>
   map <C-g> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>

It is then possible to navigate inside the code from vim
and to use auto-completion of functions and variables when writing code.

* For opening the file containing the definition of the function or variable 
   under the cursor use ``<Ctrl-G>`` (vertical split) or ``<Ctrl-H>`` 
   (horizontal split).

* For auto-completion, use either ``<Ctrl-N>`` or ``<Ctrl-P>``.

When pasting already indented code press ``<F8>`` to enter paste mode and avoid
extra indentation. It works with the line 

.. code-block:: vim

   set pastetoggle=<F8>

in ``.vimrc``.

For spell checking, I have set the following

.. code-block:: vim

   map <silent> <F7> "<Esc>:silent setlocal spell! spelllang=en<CR>"
   map <silent> <F6> "<Esc>:silent setlocal spell! spelllang=fr<CR>"


when spell checl is set, correction suggestions are available by typing ``z=``
in command mode.

Create hotspot on linux mint
----------------------------

If you have wired connection, then do this:

#. Go to "Network Connections" from bottom-corner networking icon.

#. Click on "Add" and choose "Wi-Fi" option.

#. Give the connection name (to adapter not SSID).

#. Provide a name of your hotspot in SSID field.

#. Set mode to Hotspot.

#. Select the (only?) availeble option under "Device". wlp2s0 (48:51:B7:71:D4:37)

#. Go to "Wi-Fi Security" and choose "WPA & WPA2 personal" or any type of security of your preference and set up a good password.

#. Now, go to "IPv4 Settings" and set the "Mode" to "Shared to other computers".

#. Save your settings and connect your hotspot from "Connect to Hidden Network..." and you're done.



