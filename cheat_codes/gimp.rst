GIMP
====

This section contains litle tricks useful with the illustration software GIMP_.

.. _GIMP: https://www.gimp.org/

Autocrop all layers
-------------------

Open a bunch of images as layers in gimp, then open the Python-Fu console,
``Filters -> Python-Fu -> Console``. Finally paste the following lines:

.. code-block:: python

    img = gimp.image_list()[0]
    for layer in img.layers:
        pdb.gimp_image_set_active_layer(img, layer)
        pdb.plug_in_autocrop_layer(img, layer)

